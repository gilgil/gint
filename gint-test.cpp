#include "gint.h"

int main() {
	//
	// default ctor
	//
	{
		GInt i;
	}

	//
	// conversion ctor
	//
	{
		GInt i(1);
	}

	{
		GInt i(1);
		GInt j(2);
	}

	//
	// conversion ctor
	//
	{
		GInt i = 1;
		GInt j = 2;
	}

	//
	// copy ctor
	//
	{
		GInt i = 1; // argument ctor
		GInt j = i; // copy ctor
	}
}
